/*
 * Copyright (C) 2016+     AzerothCore <www.azerothcore.org>, released under GNU GPL v2 license: http://github.com/azerothcore/azerothcore-wotlk/LICENSE-GPL2
 * Copyright (C) 2008-2016 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 */

/* ScriptData
Name: titles_commandscript
%Complete: 100
Comment: All titles related commands
Category: commandscripts
EndScriptData */

#include "Chat.h"
#include "Language.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "Language.h"
#include "TargetedMovementGenerator.h"                      // for HandleNpcUnFollowCommand
#include "CreatureAI.h"
#include "Pet.h"

class mount_commandscript : public CommandScript
{
public:
    mount_commandscript() : CommandScript("mount_commandscript") { }

    std::vector<ChatCommand> GetCommands() const override
    {
        static std::vector<ChatCommand> mountCommandTable =
        {
            { "set",       		 SEC_PLAYER,     false, &HandleMountSetCommand,                "" },
            { "spawn",        	 SEC_PLAYER,     false, &HandleMountSpawnCommand,        	   "" },
            { "despawn",         SEC_PLAYER,     false, &HandleMountDespawnCommand,            "" },
			{ "follow",        	 SEC_PLAYER,     false, &HandleMountFollowCommand,        	   "" },
            { "unfollow",        SEC_PLAYER,     false, &HandleMountUnfollowCommand,           "" },
			{ "distance",        SEC_PLAYER,     false, &HandleMountDistanceCommand,           "" }
        };
		
        static std::vector<ChatCommand> commandTable =
        {
            { "mount",          SEC_PLAYER,     false, nullptr,                                "", mountCommandTable }
        };
		
        return commandTable;
    }
	
	static bool HandleMountSetCommand(ChatHandler* handler, const char* args)
    {
		if (!*args)
			return false;
		
		Player* player = handler->GetSession()->GetPlayer();
		uint32 mountEntry = (uint32)atoi((char*)args);
        		
		// check if this mount is available for spawning (table world.mount_allowed)
		PreparedStatement* selectMountAllowedStatement = WorldDatabase.GetPreparedStatement(WORLD_SEL_MOUNT_ALLOWED);
		selectMountAllowedStatement->setUInt32(0, mountEntry);
        PreparedQueryResult selectMountAllowedResult = WorldDatabase.Query(selectMountAllowedStatement);
        if (!selectMountAllowedResult) {
			handler->PSendSysMessage("This mount is not allowed to be used.");
			return true;
		}
		
		// get record
		PreparedStatement* selectRecordStatement = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MOUNT);
		selectRecordStatement->setUInt32(0, player->GetGUID());
        PreparedQueryResult selectRecordResult = CharacterDatabase.Query(selectRecordStatement);
		
		float distance;
		uint32 mountGuid;
		
		
		// if there is not any record yet
        if (!selectRecordResult) {
			mountGuid = 0;
			distance = 1.0f;
			
			// insert default values
			PreparedStatement* inserDefaultStatement = CharacterDatabase.GetPreparedStatement(CHAR_INS_MOUNT_DEFAULT);
			inserDefaultStatement->setUInt32(0, player->GetGUID());
			inserDefaultStatement->setUInt32(1, mountEntry);
			inserDefaultStatement->setUInt32(2, mountGuid);
			inserDefaultStatement->setFloat(3, distance);
			CharacterDatabase.Execute(inserDefaultStatement);
		} else {
			
			// get current values
			Field* fieldsDB = selectRecordResult->Fetch();	
			mountGuid = fieldsDB[1].GetUInt32();
			distance = fieldsDB[2].GetFloat();
		}
		
		// if mount is spawned right now
		if(mountGuid != 0) {
			handler->PSendSysMessage("You have to despawn your mount before switching to another.");
			return true;
		}
		
		// update database with the new mount entry
		PreparedStatement* updateEntryStatement = CharacterDatabase.GetPreparedStatement(CHAR_UPD_MOUNT_ENTRY);
		updateEntryStatement->setUInt32(0, mountEntry);
		updateEntryStatement->setUInt32(1, player->GetGUID());	
		CharacterDatabase.Execute(updateEntryStatement);
		
		handler->PSendSysMessage("Your new mount is now successfully set and ready for spawn.");
		
        return true;
    }
	
	static bool HandleMountSpawnCommand(ChatHandler* handler,  const char* args)
    {
		Player* player = handler->GetSession()->GetPlayer();
		
		// get record
		PreparedStatement* selectRecordStatement = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MOUNT);
		selectRecordStatement->setUInt32(0, player->GetGUID());
        PreparedQueryResult selectRecordResult = CharacterDatabase.Query(selectRecordStatement);
		
		// if there is not any record yet
		if (!selectRecordResult) {
			handler->PSendSysMessage("You did not set your companion mount yet.");
			return true;
		}
		
		Field* fieldsDB = selectRecordResult->Fetch();
		uint32 mountEntry = fieldsDB[0].GetUInt32();		
		uint32 mountGuid = fieldsDB[1].GetUInt32();
		
		if(mountGuid != 0) {
			handler->PSendSysMessage("Your companion mount is already spawned.");
			return true;
		}
		
		// holy hell spawning procedure taken from .npc add command (rly retarded)
        float x = player->GetPositionX();
        float y = player->GetPositionY();
        float z = player->GetPositionZ();
        float o = player->GetOrientation();
        Map* map = player->GetMap();
		
		Creature* creature = new Creature();
        if (!creature->Create(sObjectMgr->GenerateLowGuid(HIGHGUID_UNIT), map, player->GetPhaseMaskForSpawn(), mountEntry, 0, x, y, z, o))
        {
            delete creature;
            return false;
        }

        creature->SaveToDB(map->GetId(), (1 << map->GetSpawnMode()), player->GetPhaseMaskForSpawn());

        uint32 db_guid = creature->GetDBTableGUIDLow();

        // To call _LoadGoods(); _LoadQuests(); CreateTrainerSpells()
        // current "creature" variable is deleted and created fresh new, otherwise old values might trigger asserts or cause undefined behavior
        creature->CleanupsBeforeDelete();
        delete creature;
        creature = new Creature();
        if (!creature->LoadCreatureFromDB(db_guid, map))
        {
            delete creature;
            return false;
        }
		
		// update database with new guid
		PreparedStatement* updateGuidStatement = CharacterDatabase.GetPreparedStatement(CHAR_UPD_MOUNT_GUID);
		updateGuidStatement->setUInt32(0, db_guid);
		updateGuidStatement->setUInt32(1, player->GetGUID());	
		CharacterDatabase.Execute(updateGuidStatement);

		//wtf is this?
        sObjectMgr->AddCreatureToGrid(db_guid, sObjectMgr->GetCreatureData(db_guid));
        return true;
	}
	
	static bool HandleMountDespawnCommand(ChatHandler* handler, const char* args)
    {
		Player* player = handler->GetSession()->GetPlayer();
		
		// get record
		PreparedStatement* selectRecordStatement = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MOUNT);
		selectRecordStatement->setUInt32(0, player->GetGUID());
        PreparedQueryResult selectRecordResult = CharacterDatabase.Query(selectRecordStatement);
		
		// if there is not any record yet
		if (!selectRecordResult) {
			handler->PSendSysMessage("You did not set your companion mount yet.");
			return true;
		}
		
		Field* fieldsDB = selectRecordResult->Fetch();
		uint32 mountEntry = fieldsDB[0].GetUInt32();		
		uint32 mountGuid = fieldsDB[1].GetUInt32();
		
		// if not spawned
		if(mountGuid == 0) {
			handler->PSendSysMessage("Your companion mount is not spawned.");
			return true;
		}
		
		// getting creature instance
		Creature* creature = new Creature();
			if (CreatureData const* cr_data = sObjectMgr->GetCreatureData(mountGuid)) {
					creature = player->GetMap()->GetCreature(MAKE_NEW_GUID(mountGuid, cr_data->id, HIGHGUID_UNIT));
			
			// Delete the creature
			creature->CombatStop();
			creature->DeleteFromDB();
			creature->AddObjectToRemoveList();
			
			// update database with zero guid (despawned)
			PreparedStatement* updateGuidStatement = CharacterDatabase.GetPreparedStatement(CHAR_UPD_MOUNT_GUID);
			updateGuidStatement->setUInt32(0, 0);
			updateGuidStatement->setUInt32(1, player->GetGUID());	
			CharacterDatabase.Execute(updateGuidStatement);
			
			handler->PSendSysMessage("Your companion mount has been despawned.");
			return true;
		} else {
			handler->PSendSysMessage("Error. Mount probably not spawned? Deleted by GM? Who knows.");
			return true;
		}
	}
	
	static bool HandleMountFollowCommand(ChatHandler* handler, const char* args)
    {
		Player* player = handler->GetSession()->GetPlayer();
		
		// get record
		PreparedStatement* selectRecordStatement = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MOUNT);
		selectRecordStatement->setUInt32(0, player->GetGUID());
        PreparedQueryResult selectRecordResult = CharacterDatabase.Query(selectRecordStatement);
		
		// if there is not any record yet
		if (!selectRecordResult) {
			handler->PSendSysMessage("You did not set your companion mount yet.");
			return true;
		}
		
		Field* fieldsDB = selectRecordResult->Fetch();	
		uint32 mountGuid = fieldsDB[1].GetUInt32();
		float distance = fieldsDB[2].GetFloat();
		
		// if not spawned -> cannot follow
		if(mountGuid == 0) {
			handler->PSendSysMessage("Your companion mount is not spawned.");
			return true;
		}
		
		// getting creature instance
		Creature* creature = new Creature();
		if (CreatureData const* cr_data = sObjectMgr->GetCreatureData(mountGuid)) {
			creature = player->GetMap()->GetCreature(MAKE_NEW_GUID(mountGuid, cr_data->id, HIGHGUID_UNIT));
			
			// following procedure taken from .npc follow
			creature->GetMotionMaster()->MoveFollow(player, distance, creature->GetFollowAngle());

			handler->PSendSysMessage(LANG_CREATURE_FOLLOW_YOU_NOW, creature->GetName().c_str());
			return true;
		} else {
			handler->PSendSysMessage("Error. Mount probably not spawned? Deleted by GM? Who knows.");
			return true;
		}
		
		
	}
	
	static bool HandleMountUnfollowCommand(ChatHandler* handler, const char* args)
    {
		Player* player = handler->GetSession()->GetPlayer();
		
		// get record
		PreparedStatement* selectRecordStatement = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MOUNT);
		selectRecordStatement->setUInt32(0, player->GetGUID());
        PreparedQueryResult selectRecordResult = CharacterDatabase.Query(selectRecordStatement);
		
		// if there is not any record yet
		if (!selectRecordResult) {
			handler->PSendSysMessage("You did not set your companion mount yet.");
			return true;
		}
		
		Field* fieldsDB = selectRecordResult->Fetch();	
		uint32 mountGuid = fieldsDB[1].GetUInt32();
		
		// if not spawned -> cannot follow
		if(mountGuid == 0) {
			handler->PSendSysMessage("Your companion mount is not spawned.");
			return true;
		}
		
		// getting creature instance
		Creature* creature = new Creature();
		if (CreatureData const* cr_data = sObjectMgr->GetCreatureData(mountGuid)) {
			creature = player->GetMap()->GetCreature(MAKE_NEW_GUID(mountGuid, cr_data->id, HIGHGUID_UNIT));
			
			// some following stuff taken from .npc unfollow
			if (creature->GetMotionMaster()->GetCurrentMovementGeneratorType() != FOLLOW_MOTION_TYPE)
			{
				handler->PSendSysMessage(LANG_CREATURE_NOT_FOLLOW_YOU, creature->GetName().c_str());
				handler->SetSentErrorMessage(true);
				return false;
			}

			FollowMovementGenerator<Creature> const* mgen = static_cast<FollowMovementGenerator<Creature> const*>((creature->GetMotionMaster()->top()));

			if (mgen->GetTarget() != player)
			{
				handler->PSendSysMessage(LANG_CREATURE_NOT_FOLLOW_YOU, creature->GetName().c_str());
				handler->SetSentErrorMessage(true);
				return false;
			}
			// some stuff ends

			// reset movement
			creature->GetMotionMaster()->MovementExpired(true);

			handler->PSendSysMessage(LANG_CREATURE_NOT_FOLLOW_YOU_NOW, creature->GetName().c_str());
			return true;
		} else {
			handler->PSendSysMessage("Error. Mount probably not spawned? Deleted by GM? Who knows.");
			return true;
		}
	}
	
	static bool HandleMountDistanceCommand(ChatHandler* handler, const char* args)
    {		
		if (!*args)
            return false;

		float distance = (float)atof((char*)args);
        if (distance > 10.0f || distance < 0.1f)
        {
            handler->SendSysMessage(LANG_BAD_VALUE);
            handler->SetSentErrorMessage(true);
            return false;
        }
		
		Player* player = handler->GetSession()->GetPlayer();
		
		PreparedStatement* updateDistanceStatement = CharacterDatabase.GetPreparedStatement(CHAR_UPD_MOUNT_DISTANCE);
		updateDistanceStatement->setFloat(0, distance);
		updateDistanceStatement->setUInt32(1, player->GetGUID());	
		CharacterDatabase.Execute(updateDistanceStatement);
	}
	
	//player->GetMap()->GetCreature(guid);
	
	//npc follow handling
    /*static bool HandleNpcFollowCommand(ChatHandler* handler, const char*)
    {
        Player* player = handler->GetSession()->GetPlayer();
        Creature* creature = handler->getSelectedCreature();

        if (!creature)
        {
            handler->PSendSysMessage(LANG_SELECT_CREATURE);
            handler->SetSentErrorMessage(true);
            return false;
        }

        creature->GetMotionMaster()->MoveFollow(player, PET_FOLLOW_DIST, creature->GetFollowAngle());

        handler->PSendSysMessage(LANG_CREATURE_FOLLOW_YOU_NOW, creature->GetName().c_str());
        return true;
    }*/

};

void AddSC_mount_commandscript()
{
    new mount_commandscript();
}
