/***********************************************************************************
*                                                                                  *
*                              Created by,                                         *
*                              Matej Lyran Kucera                                  *
*                                                                                  *
************************************************************************************/
#include "ScriptMgr.h"


class gob_teleport : public GameObjectScript
{
	public:
		gob_teleport() : GameObjectScript("gob_teleport") { }

    bool OnGossipHello(Player* player, GameObject* go) override
    {
        uint32 entry = go->GetEntry();
		
		if(player->IsInFlight()) {
			return false;
		}
		
		// zjistim souradnice kam ma gob portnout
		// SELECT map,x,y,z,o FROM gameobject_teleport WHERE entry = ?
		PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_GOB_TELEPORT);
		stmt->setUInt32(0, entry);
		PreparedQueryResult result = WorldDatabase.Query(stmt);
		if (result)
		{
			Field* fields  = result->Fetch();
			uint32 map = uint32(fields[0].GetUInt64());
			float x = fields[1].GetFloat();
			float y = fields[2].GetFloat();
			float z = fields[3].GetFloat();
			float o = fields[4].GetFloat();
			
			player->TeleportTo(map, x, y, z, o); 
			return true;			
		} else {
			return false;
		}
		
		return false;

    }
};

void AddSC_gob_teleport()
{
        new gob_teleport();
}